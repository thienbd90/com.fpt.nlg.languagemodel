package com.fpt.fti.nlg.languagemodel.util;

import java.util.Properties;

public class TokenizerProps {
	public static Properties properties = new Properties();
	static {
		properties.put("lexiconDFA",
				"resources/models/tokenization/automata/dfaLexicon.xml");
		properties.put("externalLexicon",
				"resources//models/tokenization/automata/externalLexicon.xml");
		properties.put("normalizationRules",
				"resources/models/tokenization/normalization/rules.txt");
		properties.put("lexers",
				"resources/models/tokenization/lexers/lexers.xml");
		properties.put("unigramModel",
				"resources/models/tokenization/bigram/unigram.xml");
		properties.put("bigramModel",
				"resources/models/tokenization/bigram/bigram.xml");
		properties.put("namedEntityPrefix",
				"resources/models/tokenization/prefix/namedEntityPrefix.xml");
		properties.put("sentDetectionModel",
				"resources/models/sentDetection/VietnameseSD.bin.gz");
	}
}
