package com.fpt.fti.nlg.languagemodel;

import java.util.List;

import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.collections.BoundedList;
import edu.berkeley.nlp.lm.util.Annotations.OutputParameter;

public interface ContextEncodedNgramLanguageModel<String> extends NgramLanguageModel<String>{
	/**
	 * Simple class for returning context offsets
	 * 
	 * @author adampauls
	 * 
	 */
	public static class LmContextInfo
	{

		/**
		 * Offset of context (prefix) of an n-gram
		 */
		public long offset = -1L;

		/**
		 * The (0-based) length of <code>context</code> (i.e.
		 * <code>order == 0</code> iff <code>context</code> refers to a
		 * unigram).
		 * 
		 * Use -1 for an empty context.
		 */
		public int order = -1;

	}

	/**
	 * Get the score for an n-gram, and also get the context offset of the
	 * n-gram's suffix.
	 * 
	 * @param contextOffset
	 *            Offset of context (prefix) of an n-gram
	 * @param contextOrder
	 *            The (0-based) length of <code>context</code> (i.e.
	 *            <code>order == 0</code> iff <code>context</code> refers to a
	 *            unigram).
	 * @param word
	 *            Last word of the n-gram
	 * @param outputContext
	 *            Offset of the suffix of the input n-gram. If the parameter is
	 *            <code>null</code> it will be ignored. This can be passed to
	 *            future queries for efficient access.
	 * @return
	 */
	public float getLogProb(long contextOffset, int contextOrder, int word, @OutputParameter LmContextInfo outputContext);

	/**
	 * Gets the offset which refers to an n-gram. If the n-gram is not in the
	 * model, then it returns the shortest suffix of the n-gram which is. This
	 * operation is not necessarily fast.
	 * 
	 */
	public LmContextInfo getOffsetForNgram(int[] ngram, int startPos, int endPos);

	/**
	 * Gets the n-gram referred to by a context-encoding. This operation is not
	 * necessarily fast.
	 * 
	 */
	public int[] getNgramForOffset(long contextOffset, int contextOrder, int word);

	public static class DefaultImplementations
	{

		public static <T> float scoreSentence(final List<T> sentence, final ContextEncodedNgramLanguageModel<T> lm) {
			final List<T> sentenceWithBounds = new BoundedList<T>(sentence, lm.getWordIndexer().getStartSymbol(), lm.getWordIndexer().getEndSymbol());

			final int lmOrder = lm.getLmOrder();
			float sentenceScore = 0.0f;
			for (int i = 1; i < lmOrder - 1 && i <= sentenceWithBounds.size() + 1; ++i) {
				final List<T> ngram = sentenceWithBounds.subList(-1, i);
				final float scoreNgram = lm.getLogProb(ngram);
				sentenceScore += scoreNgram;
			}
			for (int i = lmOrder - 1; i < sentenceWithBounds.size() + 2; ++i) {
				final List<T> ngram = sentenceWithBounds.subList(i - lmOrder, i);
				final float scoreNgram = lm.getLogProb(ngram);
				sentenceScore += scoreNgram;
			}
			return sentenceScore;
		}

		public static <T> float getLogProb(final List<T> ngram, final ContextEncodedNgramLanguageModel<T> lm) {
			final LmContextInfo contextOutput = new LmContextInfo();
			final WordIndexer<T> wordIndexer = lm.getWordIndexer();
			float score = Float.NaN;
			for (int i = 0; i < ngram.size(); ++i) {
				score = lm.getLogProb(contextOutput.offset, contextOutput.order, wordIndexer.getIndexPossiblyUnk(ngram.get(i)), contextOutput);
			}
			return score;
		}// -Score of "mỗi_m... " là: -5.672220230102539

	}
}
