package com.fpt.fti.nlg.languagemodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import vn.hus.nlp.tokenizer.VietTokenizer;
import com.fpt.fti.nlg.languagemodel.util.TokenizerProps;
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.io.LmReaders;

public class LanguageModel {
	
	/**
	 * The underlying tokenizer
	 */
	public static VietTokenizer tokenizer = new VietTokenizer(TokenizerProps.properties);
	
	// 
	private  NgramLanguageModel<String> ngramModel = null;
	
	/**
	 * Default constructor
	 */
	public LanguageModel() {}
	
	
	/**
	 * Initializes the ngram language model using a model file.
	 * @param modelFile a ngram model.
	 */
	public LanguageModel(String modelFile) {
		long start = System.currentTimeMillis();
		ngramModel = LmReaders.readLmBinary(modelFile);
		System.err.println("Loading n-gram model...completed [" + (System.currentTimeMillis()-start) +" ms]" );
	}
	
	/**
	 * A segment method, using other tools.
	 * @param sentence a sentence to be segmented
	 * @return a segmented sentence
	 */
	public String segmentText(String sentence) {
		return tokenizer.segment(sentence).trim().toLowerCase();
	}
	
	/**
	 * get list tokens of  segmented sentence
	 * @param text
	 * @return a list tokens of segmented sentence
	 */
	public List<String> sentenceTokens(String text) {
		List<String> result = new ArrayList<String>(Arrays.asList(segmentText(text).split("\\s+")));
		return result;
	}
	
	
	/**
	 * 
	 * Scores an n-gram. This is a convenience method and will generally be
	 * relatively inefficient. More efficient versions are available in
	 * .
	 */
	
	public double getLogProb(String ngram) {
		List<String> tokens = sentenceTokens(ngram);
		return ngramModel.getLogProb(tokens);
	}
	
	
	/**
	 * Scores a complete sentence, taking appropriate care with the start- and
	 * end-of-sentence symbols
	 */
	public double scoreSentence(String sentence) {
		List<String> sentTokens = sentenceTokens(sentence);
		return ngramModel.scoreSentence(sentTokens);
	}
	
	// public static void main(String[] args) {
	// String input1 = "mỗi một thập niên";
	// String input2 = "chủ tịch g tập đoàn";
	// LanguageModel languageModel = new
	// LanguageModel("resources/models/binaryLM/binaryLM.bin");
	//
	// System.err.println("-Score of \"" + input1.substring(0,input1.length()/3)
	// + "... \" is: " + languageModel.getLogProb(input1));
	//
	// System.err.println("-Score of \"" + input2.substring(0,input2.length()/3)
	// + "... \" is: " + languageModel.scoreSentence(input2));
	// }
	
	
}
