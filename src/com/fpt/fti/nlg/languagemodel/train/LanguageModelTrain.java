package com.fpt.fti.nlg.languagemodel.train;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vn.hus.nlp.tokenizer.VietTokenizer;

import com.fpt.fti.nlg.languagemodel.util.TokenizerProps;

import edu.berkeley.nlp.lm.ConfigOptions;
import edu.berkeley.nlp.lm.NgramLanguageModel;
import edu.berkeley.nlp.lm.StringWordIndexer;
import edu.berkeley.nlp.lm.io.ArpaLmReader;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.util.Logger;

public class LanguageModelTrain {


	// public static void main(String[] args) {
	// //trainFolder();
	// }
	
	final static StringWordIndexer wordIndexer = new StringWordIndexer();

	static {
		wordIndexer.setStartSymbol(ArpaLmReader.START_SYMBOL);
		wordIndexer.setEndSymbol(ArpaLmReader.END_SYMBOL);
		wordIndexer.setUnkSymbol(ArpaLmReader.UNK_SYMBOL);
	}

	private enum Opts {
		HASH_OPT {
			@Override
			public String toString() {
				return "-h";
			}

			@Override
			public String docString() {
				return "build an array-encoded hash-table LM (the default)";
			}

			@Override
			public NgramLanguageModel<String> makeLm(final String file) {
				return LmReaders.readArrayEncodedLmFromArpa(file, false);
			}
		},
		CONTEXT_OPT {
			@Override
			public String toString() {
				return "-e";
			}

			@Override
			public String docString() {
				return "build a context-encoded LM instead of the default hash table";
			}

			@Override
			public NgramLanguageModel<String> makeLm(final String file) {
				return LmReaders.readContextEncodedLmFromArpa(file);
			}
		},
		COMPRESS_OPT {
			@Override
			public String toString() {
				return "-c";
			}

			@Override
			public String docString() {
				return "build a compressed hash-table LM instead of the array encoding";
			}

			@Override
			public NgramLanguageModel<String> makeLm(final String file) {
				return LmReaders.readArrayEncodedLmFromArpa(file, true);
			}
		};

		public abstract String docString();

		public abstract NgramLanguageModel<String> makeLm(String file);
	}

	public static void trainFile(String file) {
		//
	}

	public static void trainFolder(String folder, String modelName) {

		List<String> files = new ArrayList<String>();
		File inputFolder = new File(folder);
		File[] temps = inputFolder.listFiles();
		for (File s : temps) {
			files.add(s.getAbsolutePath());
			System.err.println(s.getAbsolutePath());
			if(files.size() == 200) break;
		}
		LmReaders.createKneserNeyLmFromTextFiles(files, wordIndexer, 2,
				new File(modelName),
				new ConfigOptions());
		System.err.println("done");
		Logger.setGlobalLogger(new Logger.SystemLogger(System.out, System.err));
		final String lmFile = modelName;
		Opts finalOpt = Opts.HASH_OPT;
		Logger.startTrack("Reading Lm File " + lmFile + " . . . ");
		final NgramLanguageModel<String> lm = finalOpt.makeLm(lmFile);
		Logger.endTrack();
		final String outFile = "resources/models/binaryLM.bin";
		Logger.startTrack("Writing to file " + outFile + " . . . ");
		LmReaders.writeLmBinary(lm, outFile);
		Logger.endTrack();

	}
}
