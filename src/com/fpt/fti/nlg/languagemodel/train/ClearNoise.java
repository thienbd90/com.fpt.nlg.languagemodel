package com.fpt.fti.nlg.languagemodel.train;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClearNoise {
	
//	public static String PROCESSED_DIR = "processed";
//	public static String DATA_DIR = "data";
	//public static VietnameseMaxentTagger tagger = new VietnameseMaxentTagger();

	
	public static void main(String[] args) {
		String PROCESSED_DIR = "processed";
		String DATA_DIR = "data";
		process(DATA_DIR, PROCESSED_DIR);
	}
	
	
	public static void process(String DATA_DIR, String PROCESSED_DIR) {
		File dir = new File(DATA_DIR);
		String[] files = dir.list();
		
		for (String fileName : files) {
			String filePath = DATA_DIR + "\\" + fileName ;
			List<String> lines = readFile(filePath);
			witeFile(lines, fileName, PROCESSED_DIR);
		}
		
		System.err.println("Processed total " + files.length + " files");
	}

	private static List<String> readFile(String fileName) {
		List<String> lines = new ArrayList<String>();
		try {
			File file = new File(fileName);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				//System.err.println(line);
				// process the line
				//line = line.toLowerCase();
				if(line.equals("")) { System.err.println("Here"); continue ;}
				line = line.replaceAll("< < a", "");
				line = line.replaceAll("< < t", "");
				line = line.replaceAll("< < c", "");
				line = line.replaceAll("/ t > >", "");
				line = line.replaceAll("/ a > >", "");
				line = line.replaceAll("/ c > >", ""); //next_page > >
				line = line.replaceAll("next_page > >", "");
				line = line.replaceAll("Next_page > >", "");
				line = line.replaceAll("more : ", "");
				line = line.replaceAll("\\.", "");
				line = line.replaceAll("\\(", "");
				line = line.replaceAll("\\)", "");
				line = line.replaceAll(",", "");
				line = line.replaceAll("\"", "");
				line = line.replaceAll("<", "");
				line = line.replaceAll(">", "");
				line = line.replaceAll("-", "");
				line = line.replaceAll("!", "");
				line = line.replaceAll("\\?", ""); //
				line = line.replaceAll("“", "");
				line = line.replaceAll("”", "");
				line = line.replaceAll(":", "");
				line = line.replaceAll("\\*", "");
				//line = line.replaceAll("", "");
				//line = tagger.tagText(line.replaceAll("_", " "));
				//line = line.replaceAll("", "");
				if(line.trim().equals("") || line.contains("|") || line.toLowerCase().contains("fontfamily")) { System.err.println("Here :" + line); continue ;}
				lines.add(line.trim().toLowerCase());
				//System.err.println(line.trim().toLowerCase());
			}
			br.close();
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}

	public static void witeFile(List<String> lines, String fileName, String dir) {
		try {
			File file = new File(dir + "/" + fileName);
//			if (!file.exists()) {
//				file.createNewFile();
//			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for (String line : lines) {
				bw.write(line + "\n");
			}
			bw.close();
			System.out.println("Process " + fileName + "... completed!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
